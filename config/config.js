const config = {
  menu: [
    {
      titulo: "Finanzas",
      enlace: "/",
    },
  ],
  competiciones: [
    { id: 8, nombre: "LaLiga", slug: "laliga" },
    { id: 17, nombre: "Premier League", slug: "premier-league" },
    { id: 34, nombre: "Ligue 1", slug: "ligue-1" },
    { id: 23, nombre: "Serie A", slug: "serie-a" },
  ],
  anios: ["20-21", "19-20"],
  apiURL: "http://localhost:3002/api/",
};

export default config;
