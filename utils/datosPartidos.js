export const ordenarSegunTiempo = (datos) => {
  const copy = [...datos];
  copy.sort((a, b) => {
    if (a.minuto + a.extra > b.minuto + b.extra) {
      return 1;
    }
    if (a.minuto + a.extra < b.minuto + b.extra) {
      return -1;
    }
    return 0;
  });
  return copy;
};

export const colorGanador = (ganador, local) => {
  if (ganador === 1 && local) {
    return "green";
  } else if (ganador === 2 && !local) {
    return "green";
  } else if (ganador === 3) {
    return "orange";
  }
  return "red";
};
