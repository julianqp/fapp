import axios from "axios";
import config from "../config/config";
export const createFile = async (info) => {
  const { data } = await axios.post(`${config.apiURL}files`, info);
};
