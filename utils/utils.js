const util = require("util");

export const imprimir = (datos) => {
  console.log(util.inspect({ datos }, false, null, true));
};
