import React from "react";
import { Menu } from "antd";
import {
  DesktopOutlined,
  PieChartOutlined,
  FileOutlined,
  TeamOutlined,
  MobileFilled,
  SmileOutlined,
  UserOutlined,
  CopyFilled,
  SwapLeftOutlined,
  RadiusBottomleftOutlined,
  TrophyFilled,
} from "@ant-design/icons";
import Link from "next/link";
import { useRouter } from "next/router";
import config from "../config/config";
import { AiTwotoneHdd } from "react-icons/ai";

const { SubMenu } = Menu;

const MenuComponent = () => {
  const router = useRouter();

  return (
    <Menu theme="dark" defaultSelectedKeys={[router.asPath]} mode="inline">
      <SubMenu
        key="competiciones"
        icon={<TrophyFilled />}
        title="Competiciones"
      >
        {config.competiciones.map((item) => {
          return (
            <Menu.Item key={`/competicion/${item.slug}`}>
              <Link href="/competicion/[slug]" as={`/competicion/${item.slug}`}>
                {item.nombre}
              </Link>
            </Menu.Item>
          );
        })}
      </SubMenu>
      <SubMenu key="tarjetas" icon={<MobileFilled />} title="Tarjetas">
        {config.anios.map((item) => {
          return (
            <Menu.Item key={`/tarjeta/${item}`}>
              <Link href="/tarjetas/[anio]" as={`/tarjetas/${item}`}>
                {item}
              </Link>
            </Menu.Item>
          );
        })}
      </SubMenu>
      <SubMenu
        key="corners"
        icon={<RadiusBottomleftOutlined />}
        title="Corners"
      >
        {config.anios.map((item) => {
          return (
            <Menu.Item key={`/corners/${item}`}>
              <Link href="/corners/[anio]" as={`/corners/${item}`}>
                {item}
              </Link>
            </Menu.Item>
          );
        })}
      </SubMenu>
      <Menu.Item icon={<UserOutlined />} key={`/admin`}>
        <Link href="/admin">Admin</Link>
      </Menu.Item>
    </Menu>
  );
};

export default MenuComponent;
