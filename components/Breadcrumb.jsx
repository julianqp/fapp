import { Breadcrumb } from "antd";
import { useRouter, Router } from "next/router";

const BreadcrumbComponent = () => {
  const { asPath } = useRouter();
  const partes = asPath.split("/");

  return (
    <Breadcrumb style={{ margin: "16px 0" }}>
      <Breadcrumb.Item> Home </Breadcrumb.Item>
      {partes.map((item) => {
        return <Breadcrumb.Item key={item}>{item}</Breadcrumb.Item>;
      })}
    </Breadcrumb>
  );
};

export default BreadcrumbComponent;
