import { Card } from "antd";
import { useRouter } from "next/router";

const { Meta } = Card;

const CardEquipo = ({ equipo }) => {
  const { id, nombre, _id } = equipo;
  const router = useRouter();
  return (
    <Card
      onClick={() => router.push("/equipo/[equipo]", `/equipo/${equipo.slug}`)}
      hoverable
      style={{ width: 150, margin: 5, padding: 2, paddingTop: 5 }}
      cover={<img alt="equipo" src={`/equipos/${id}.png`} />}
    >
      <p className="text-center text-xs">{nombre}</p>
    </Card>
  );
};

export default CardEquipo;
