import React, { useState, useEffect } from "react";
import { Table } from "antd";
import { Switch } from "@material-ui/core";
import axios from "axios";
import config from "../../config/config";
import { useRouter } from "next/router";
import { indigo } from "@material-ui/core/colors";

const UltimosPartidos = () => {
  const [partidos, setPartidos] = useState([]);
  const router = useRouter();
  const { slug, anio } = router.query;

  const obtenerPartidos = async () => {
    const { data } = await axios.get(
      `${config.apiURL}partidos/competicion/${slug}/${anio}`
    );
    const partidos = data.data.map((x) => ({ ...x, key: x._id }));
    setPartidos(partidos);
  };

  useEffect(() => {
    console.log("Ahora");
    obtenerPartidos();
  }, []);

  if (partidos.length === 0) {
    return null;
  }
  const columns = [
    {
      align: "center",
      title: "Fecha",
      dataIndex: "fecha",
      width: 200,
      render: (fecha) => new Date(fecha).toLocaleString("es-ES"),
    },
    {
      align: "center",
      title: "Local",
      dataIndex: "local",
      width: 150,
      render: (row) => row.nombre,
    },
    {
      align: "center",
      title: "Visitante",
      dataIndex: "visitante",
      width: 150,
      render: (row) => row.nombre,
    },
    {
      align: "center",
      title: "Resultado",
      dataIndex: "resultado",
      render: (row) => {
        return `${row.local.final} - ${row.visitante.final}`;
      },
    },
    {
      align: "center",
      title: "Ganador",
      width: 150,
      render: (row) => {
        if (row.ganador === 1) {
          return row.local.nombre;
        } else if (row.ganador === 2) {
          return row.visitante.nombre;
        }
        return "Empate";
      },
    },
  ];
  return (
    <Table
      onRow={(record, rowIndex) => {
        return {
          onClick: () => {
            console.log({ record });
            const anioParse = anio.replace("/", "-");
            return router.push(
              "/competicion/[slug]/anio/[anio]/partido/[partido]",
              `/competicion/${slug}/anio/${anioParse}/partido/${record.slug}`
            );
          }, // click row
        };
      }}
      key="tabla"
      columns={columns}
      dataSource={partidos}
      pagination={{
        defaultPageSize: 5,
        pageSizeOptions: [5, 10, 15],
      }}
      scroll={{ y: 240 }}
    />
  );
};

export default UltimosPartidos;
