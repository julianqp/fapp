import React from "react";
import { List, Row, Col, Avatar } from "antd";
import {
  BorderOutlined,
  TabletFilled,
  setTwoToneColor,
} from "@ant-design/icons";

const ListaDatosPartido = ({ datos, tarjeta }) => {
  const listaDatos = (lista) => {
    return (
      <List
        itemLayout="horizontal"
        dataSource={lista}
        renderItem={(item) => {
          const iniciales = item.jugador.nombre
            .split(" ")
            .map((elem) => elem.charAt(0));
          const color = item.isLocal ? "blue" : "red";
          let colorTajeta;
          if (tarjeta) {
            switch (item.razon) {
              case "yellow":
                colorTajeta = (
                  <span className="pl-3">
                    <TabletFilled
                      theme="filled"
                      style={{ fontSize: "16px", color: "#CACA00" }}
                    />
                  </span>
                );

                break;
              case "red":
                colorTajeta = (
                  <span className="pl-3">
                    <TabletFilled
                      theme="filled"
                      style={{ fontSize: "16px", color: "#CA0000" }}
                    />
                  </span>
                );

                break;
              case "yellowRed":
                colorTajeta = (
                  <span className="pl-3">
                    <TabletFilled
                      theme="filled"
                      style={{ fontSize: "16px", color: "#CACA00" }}
                    />
                    <TabletFilled
                      theme="filled"
                      style={{ fontSize: "16px", color: "#CA0000" }}
                    />
                  </span>
                );
                break;
              default:
                break;
            }
          }
          return (
            <List.Item className="pr-8">
              <List.Item.Meta
                avatar={
                  <Avatar style={{ backgroundColor: color }}>
                    {iniciales.map((x) => x)}
                  </Avatar>
                }
                title={
                  <p>
                    {item.jugador.nombre}
                    {tarjeta ? colorTajeta : null}
                  </p>
                }
                description={<p>Minuto: {item.minuto + item.extra}</p>}
              />
              <p className="pr-12">Resultado: {item.resultado}</p>
            </List.Item>
          );
        }}
      />
    );
  };

  let rLocal = 0;
  let rVisitante = 0;
  const local = [];
  const visitante = [];
  datos.map((elem) => {
    const copy = { ...elem };
    if (elem.isLocal) {
      rLocal += 1;
      copy.resultado = `${rLocal} - ${rVisitante}`;
      local.push(copy);
    } else {
      rVisitante += 1;
      copy.resultado = `${rLocal} - ${rVisitante}`;
      visitante.push(copy);
    }
  });

  return (
    <Row justify="center">
      <Col span={6}>{listaDatos(local)}</Col>
      <Col span={6}>{listaDatos(visitante)}</Col>
    </Row>
  );
};

export default ListaDatosPartido;
