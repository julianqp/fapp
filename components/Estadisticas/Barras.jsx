import React from "react";
import { Bar } from "react-chartjs-2";

const Barras = ({ options, datos }) => {
  return <Bar data={datos} width={100} height={30} options={options} />;
};

export default Barras;
