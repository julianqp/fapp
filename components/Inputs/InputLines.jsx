import React from "react";
import { Input } from "antd";

const { TextArea } = Input;

const InputLines = ({ value, onChange }) => {
  return (
    <TextArea
      value={value}
      onChange={(e) => onChange(e.target.value)}
      className="bg-black"
      rows={4}
    />
  );
};
export default InputLines;
