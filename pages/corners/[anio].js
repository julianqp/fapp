import React, { createElement, useState } from "react";
import axios from "axios";
import Layout from "../../components/Layout";
import { Row, Col, Collapse, Avatar } from "antd";
import config from "../../config/config";
import { imprimir } from "../../utils/utils";
import Link from "next/link";
const { Panel } = Collapse;
import { useRouter } from "next/router";

const text = `
  A dog is a type of domesticated animal.
  Known for its loyalty and faithfulness,
  it can be found as a welcome guest in many households across the world.
`;

const CornersAnio = ({ competiciones }) => {
  const [likes, setLikes] = useState(0);
  const [dislikes, setDislikes] = useState(0);
  const [action, setAction] = useState(null);
  const router = useRouter();
  const { anio } = router.query;
  const like = () => {
    setLikes(1);
    setDislikes(0);
    setAction("liked");
  };

  const dislike = () => {
    setLikes(0);
    setDislikes(1);
    setAction("disliked");
  };

  return (
    <div className="full-space">
      <Layout>
        <Collapse accordion>
          {competiciones && competiciones.length
            ? competiciones.map((elem, index) => {
                return (
                  <Panel header={elem.nombre} key={index + 1}>
                    {elem.equipos && elem.equipos.length ? (
                      <Row>
                        {elem.equipos.map((item) => {
                          return (
                            <Col
                              key={`equipo-${item.slug}`}
                              xs={{ span: 8, offset: 1 }}
                              lg={{ span: 6, offset: 2 }}
                              xl={{ span: 4, offset: 2 }}
                            >
                              <Link
                                href="/corners/[anio]/[competicion]/[slug]"
                                as={`/corners/${anio}/${elem.slug}/${item.slug}`}
                              >
                                <div className="botones">
                                  <Avatar
                                    src={`/equipos/${item.id}.png`}
                                    alt="Han Solo"
                                  />
                                  <p className="equipo">{item.nombre}</p>
                                </div>
                              </Link>
                            </Col>
                          );
                        })}
                      </Row>
                    ) : (
                      <p>No hay equipos disponibles</p>
                    )}
                  </Panel>
                );
              })
            : null}
        </Collapse>
      </Layout>
    </div>
  );
};

CornersAnio.getInitialProps = async (params) => {
  const { anio } = params.query;

  const { data } = await axios.get(`${config.apiURL}tarjetas/anio/${anio}`);

  if (data.OK) {
    return {
      competiciones: data.datos,
    };
  }
  return { data: {} };
};
export default CornersAnio;
