import React, { useState, useEffect } from "react";
import Layout from "../../../../components/Layout";
import CardEquipo from "../../../../components/CardEquipo";
import { useRouter } from "next/router";
import config from "../../../../config/config";
import axios from "axios";
import UltimosPartidos from "../../../../components/Tablas/UltimosPartidos";

import { Descriptions, Divider, Card, Col, Row } from "antd";
import { Table, Typography } from "antd";

const { Text } = Typography;

const columns = [
  {
    title: "Jornada",
    dataIndex: "jornada",
  },
  {
    title: "Primera parte",
    dataIndex: "parte1",
  },
  {
    title: "Segunda parte",
    dataIndex: "parte2",
  },
  {
    title: "Total",
    render: (row) => row.parte1 + row.parte2,
  },
  {
    title: "Media",
    render: (row) =>
      Math.round(((row.parte1 + row.parte2) / row.partidos) * 100) / 100,
  },
];

const DatoTabla = ({ datos }) => {
  return (
    <Table
      columns={columns}
      dataSource={datos}
      pagination={false}
      bordered
      summary={(pageData) => {
        let totalparte1 = 0;
        let totalparte2 = 0;

        pageData.forEach(({ parte1, parte2 }) => {
          totalparte1 += parte1;
          totalparte2 += parte2;
        });

        return (
          <>
            <Table.Summary.Row>
              <Table.Summary.Cell>Total</Table.Summary.Cell>
              <Table.Summary.Cell>
                <Text type="danger">{totalparte1}</Text>
              </Table.Summary.Cell>
              <Table.Summary.Cell>
                <Text>{totalparte2}</Text>
              </Table.Summary.Cell>
            </Table.Summary.Row>
            <Table.Summary.Row>
              <Table.Summary.Cell>Balance</Table.Summary.Cell>
              <Table.Summary.Cell colSpan={2}>
                <Text type="danger">{totalparte1 + totalparte2}</Text>
              </Table.Summary.Cell>
            </Table.Summary.Row>
          </>
        );
      }}
    />
  );
};

const Anio = ({ info }) => {
  const [datosLiga, setDatosLiga] = useState([]);
  const router = useRouter();
  const { anio, slug } = router.query;

  const obtenerPartidos = async () => {
    const { data } = await axios.get(
      `${config.apiURL}competiciones/obtenerdatos/${slug}/${anio}`
    );

    if (data.OK) {
      setDatosLiga(data.data);
    }
  };

  useEffect(() => {
    console.log("Ahora");
    obtenerPartidos();
  }, []);

  if (!info) {
    return null;
  }

  return (
    <div className="full-space">
      <Layout>
        <>
          <Descriptions title={info.nombre} size="small" column={1}>
            <Descriptions.Item label="Anio">{info.anio}</Descriptions.Item>
            <Descriptions.Item label="Pais">{info.pais}</Descriptions.Item>
          </Descriptions>
        </>
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
          <Col className="gutter-row" span={8}>
            <div>
              <Divider orientation="left">Corners</Divider>
              <DatoTabla datos={datosLiga.corners} />
            </div>
          </Col>
          <Col className="gutter-row" span={8}>
            <div>
              <Divider orientation="left">Entradas</Divider>
              <DatoTabla datos={datosLiga.entradas} />
            </div>
          </Col>
          <Col className="gutter-row" span={8}>
            <div>
              <Divider orientation="left">Disparos</Divider>
              <DatoTabla datos={datosLiga.disparos} />
            </div>
          </Col>
        </Row>
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
          <Col className="gutter-row" span={8}>
            <div>
              <Divider orientation="left">Amarillas</Divider>
              <DatoTabla datos={datosLiga.amarillas} />
            </div>
          </Col>
          <Col className="gutter-row" span={8}>
            <div>
              <Divider orientation="left">Rojas</Divider>
              <DatoTabla datos={datosLiga.rojas} />
            </div>
          </Col>
        </Row>
        <div className="site-card-wrapper">
          <Divider orientation="left">Ultimos partidos</Divider>
          <UltimosPartidos />
        </div>
        <div className="site-card-wrapper">
          <Divider orientation="left">Equipos</Divider>
          <Row justify="center" gutter={16} className="centrado-filas">
            {info.equipos.map((equipo) => (
              <CardEquipo key={equipo._id} equipo={equipo} />
            ))}
          </Row>
        </div>
      </Layout>
    </div>
  );
};
export default Anio;

export async function getStaticProps({ params, preview = false }) {
  const { anio, slug } = params;

  const { data } = await axios.get(
    `${config.apiURL}competiciones/${slug}/${anio}`
  );

  if (data.OK) {
    return {
      props: {
        info: data.data,
      },
    };
  }

  return {
    props: { info: null },
  };
}

export async function getStaticPaths() {
  const { anios, competiciones } = config;
  const paths = [];

  anios.forEach((anio) =>
    competiciones.forEach(({ slug }) =>
      paths.push(`/competicion/${slug}/anio/${anio}`)
    )
  );
  return {
    paths,
    fallback: true,
  };
}
