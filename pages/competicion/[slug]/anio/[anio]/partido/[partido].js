import React, { useState } from "react";
import Layout from "../../../../../../components/Layout";
import ListaDatosPartido from "../../../../../../components/Elementos/ListaDatosPartido";
import axios from "axios";
import config from "../../../../../../config/config";
import {
  List,
  Row,
  Switch,
  Descriptions,
  Tag,
  Col,
  Avatar,
  Divider,
} from "antd";
import {
  ordenarSegunTiempo,
  colorGanador,
} from "../../../../../../utils/datosPartidos";

const Partido = ({ data }) => {
  const [checked, setChecked] = useState(false);

  const fecha = data.fecha ? new Date(data.fecha).toLocaleString("es-ES") : "";

  const goles = ordenarSegunTiempo(data.goles);
  const tarjetas = ordenarSegunTiempo(data.tarjetas);
  const onChange = (checked) => {
    setChecked(checked);
  };
  const Estadisticas = ({ parte1, parte2, all }) => {
    if (checked) {
      return (
        <div>
          <Row justify="space-around">
            <Col span={8}>
              <Divider>Todas</Divider>
              {all.map((elem) => {
                return (
                  <Descriptions key={elem._id}>
                    <Descriptions.Item key={`local-${elem._id}`}>
                      {`${elem.local}`}
                    </Descriptions.Item>
                    <Descriptions.Item
                      className="text-center"
                      key={`nombre-${elem._id}`}
                    >
                      {elem.nombre}
                    </Descriptions.Item>
                    <Descriptions.Item
                      className="text-right"
                      key={`visitante-${elem._id}`}
                    >
                      {`${elem.visitante}`}
                    </Descriptions.Item>
                  </Descriptions>
                );
              })}
            </Col>
          </Row>
        </div>
      );
    }
    return (
      <div>
        <Row justify="space-around">
          <Col className="items-center" span={8}>
            <Divider>Parte 1</Divider>
            {parte1.map((elem) => {
              return (
                <Descriptions key={elem._id}>
                  <Descriptions.Item key={`local-${elem._id}`}>
                    {`${elem.local}`}
                  </Descriptions.Item>
                  <Descriptions.Item
                    className="text-center"
                    key={`nombre-${elem._id}`}
                  >
                    {elem.nombre}
                  </Descriptions.Item>
                  <Descriptions.Item
                    className="text-right"
                    key={`visitante-${elem._id}`}
                  >
                    {`${elem.visitante}`}
                  </Descriptions.Item>
                </Descriptions>
              );
            })}
          </Col>

          <Col span={8}>
            <Divider>Parte 2</Divider>
            {parte2.map((elem) => {
              return (
                <Descriptions key={elem._id}>
                  <Descriptions.Item key={`local-${elem._id}`}>
                    {`${elem.local}`}
                  </Descriptions.Item>
                  <Descriptions.Item
                    className="text-center"
                    key={`nombre-${elem._id}`}
                  >
                    {elem.nombre}
                  </Descriptions.Item>
                  <Descriptions.Item
                    className="text-right"
                    key={`visitante-${elem._id}`}
                  >
                    {`${elem.visitante}`}
                  </Descriptions.Item>
                </Descriptions>
              );
            })}
          </Col>
        </Row>
      </div>
    );
  };

  return (
    <Layout>
      <h1>Partido</h1>
      <h2>{data.nombre}</h2>
      <div>
        <Descriptions column={3} title="Resultado Final">
          <Descriptions>
            <span
              className={`text-2xl text-${colorGanador(
                data.ganador,
                true
              )}-600`}
            >
              {data.local.nombre}
            </span>{" "}
            <Tag color="green"> {data.apuestas.local}</Tag>
          </Descriptions>
          <Descriptions>
            {data.resultado.local.final} - {data.resultado.visitante.final}
          </Descriptions>

          <Descriptions>
            <span
              className={`text-2xl text-${colorGanador(
                data.ganador,
                false
              )}-600`}
            >
              {data.visitante.nombre}
            </span>{" "}
            <Tag className="pl-" color="green">
              {data.apuestas.visitante}
            </Tag>
          </Descriptions>
        </Descriptions>
        <Descriptions title="Resultado Mitad">
          <Descriptions>{data.local.nombre}</Descriptions>
          <Descriptions>
            {data.resultado.local.mitad} - {data.resultado.visitante.mitad}
          </Descriptions>
          <Descriptions>{data.visitante.nombre}</Descriptions>
        </Descriptions>
        <Descriptions title="Datos Partido">
          <Descriptions.Item label="Liga">
            {data.competicion.nombre}
          </Descriptions.Item>
          <Descriptions.Item label="Año">
            {data.competicion.anio}
          </Descriptions.Item>
          <Descriptions.Item label="Jornada">{data.jornada}</Descriptions.Item>
          <Descriptions.Item label="Fecha">{fecha}</Descriptions.Item>
          <Descriptions.Item label="Arbitro">
            {data.arbitro.nombre}
          </Descriptions.Item>
        </Descriptions>
        <Divider orientation="left">Goles</Divider>
        <ListaDatosPartido datos={goles} />
        <Divider orientation="left">Tarjetas</Divider>
        <ListaDatosPartido datos={tarjetas} tarjeta />
        <Divider orientation="left">Estadísticas</Divider>
        <div className="text-center">
          <span className="mr-5">Partes</span>
          <Switch checked={checked} onChange={onChange} />
          <span className="ml-5">Todos</span>
        </div>

        <Estadisticas
          parte1={data.estadisticas.parte1}
          parte2={data.estadisticas.parte2}
          all={data.estadisticas.all}
        />
      </div>
    </Layout>
  );
};

Partido.getInitialProps = async (params) => {
  const { slug, anio, partido } = params.query;

  const { data } = await axios.get(
    `${config.apiURL}partidos/${slug}/${anio}/${partido}`
  );

  if (data.OK) {
    return { data: data.data };
  }
  return { data: {} };
};

export default Partido;
