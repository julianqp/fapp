import React from "react";
import Layout from "../../components/Layout";
import { useRouter } from "next/router";
import config from "../../config/config";
import axios from "axios";
import { Row, Col } from "antd";
import { Table, Tag, Space } from "antd";

const Equipo = ({ info, pais }) => {
  const router = useRouter();
  const { slug } = router.query;

  const { nombre } = config.competiciones.find((item) => item.slug === slug);
  const columns = [
    {
      title: "Año",
      dataIndex: "anio",
      key: "anio",
    },
    {
      title: "Equipos",
      dataIndex: "equipos",
      key: "equipos",
      render: (equipos) => (
        <div>
          {equipos.map((item) => {
            return (
              <Tag className="tag" key={item._id}>
                {item.nombre.toUpperCase()}
              </Tag>
            );
          })}
        </div>
      ),
    },
    {
      title: "Jornadas",
      dataIndex: "jornadas",
      key: "jornadas",
    },
  ];

  return (
    <div className="full-space">
      <Layout>
        <>
          <h1>{nombre}</h1>
          <h1>{pais}</h1>
          <Table
            rowKey={(record) => record._id}
            onRow={(record) => {
              return {
                onClick: () => {
                  const anio = record.anio.replace("/", "-");
                  return router.push(
                    "/competicion/[slug]/anio/[anio]",
                    `/competicion/${slug}/anio/${anio}`
                  );
                },
              };
            }}
            columns={columns}
            dataSource={info}
          />
        </>
      </Layout>
    </div>
  );
};
export default Equipo;

export async function getStaticProps({ params, preview = false }) {
  const { slug } = params;

  const competiciones = await axios.get(
    `${config.apiURL}competiciones/${slug}`
  );
  // console.log(competiciones);
  let info = null;
  let paiss = "";
  if (competiciones.data.OK === true) {
    info = competiciones.data.data.map(
      ({ _id, nombre, slug, id, season, pais, anio, equipos, jornadas }) => {
        paiss = pais;
        return {
          _id,
          nombre,
          slug,
          id,
          season,
          pais,
          anio,
          equipos,
          jornadas,
        };
      }
    );
  }
  console.log(paiss);
  return {
    props: {
      info,
      pais: paiss,
    },
  };
}

export async function getStaticPaths() {
  const allPosts = config.competiciones;
  return {
    paths: allPosts?.map((post) => `/competicion/${post.slug}`) || [],
    fallback: true,
  };
}
