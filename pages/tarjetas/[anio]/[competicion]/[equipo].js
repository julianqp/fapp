import React from "react";
import Layout from "../../../../components/Layout";
import axios from "axios";
import config from "../../../../config/config";
import Barras from "../../../../components/Estadisticas/Barras";
import { Table, Descriptions, Divider, Row, Col } from "antd";

const construirMedia = (arrayDatos) => {
  let actual = 0;
  return arrayDatos.map((elem, index) => {
    let newValue = 0;
    if (index === 0) {
      actual = elem;
      return actual;
    }
    newValue = (actual * index + elem) / (index + 1);
    actual = Math.round(newValue * 100) / 100;
    return actual;
  });
};

const construirMediaAritemtica = (arrayDatos) => {
  let actual = arrayDatos.reduce((acc, curr) => acc + curr);
  actual = Math.round((actual / arrayDatos.length) * 100) / 100;
  return actual;
};

const constuirOptions = (xAxes, yAxes) => {
  return {
    maintainAspectRatio: true,
    defaultFontSize: 20,
    scales: {
      yAxes: [
        {
          scaleLabel: {
            display: true,
            labelString: yAxes,
            fontSize: 20,
          },
          ticks: {
            beginAtZero: true,
          },
        },
      ],
      xAxes: [
        {
          scaleLabel: {
            display: true,
            fontSize: 20,
            labelString: xAxes,
          },
        },
      ],
    },
  };
};

const construirAtributos = (labels, data, media) => {
  return {
    labels: labels,
    datasets: [
      {
        type: "bar",
        label: "Numero tarjetas",
        backgroundColor: "rgba(255,99,132,0.2)",
        borderColor: "rgba(255,99,132,1)",
        borderWidth: 1,
        hoverBackgroundColor: "rgba(255,99,132,0.4)",
        hoverBorderColor: "rgba(255,99,132,1)",
        data: data,
      },
      {
        type: "line",
        label: "Media tarjetas",
        borderColor: "rgba(22,100,120,1)",
        borderWidth: 1,
        hoverBackgroundColor: "rgba(22,100,120,0.4)",
        hoverBorderColor: "rgba(22,100,120,1)",
        fill: false,
        data: media,
      },
    ],
  };
};

const EquipoTarjetas = ({ datos }) => {
  // Jornadas
  const labelsJornada = Object.keys(datos.tarjetasPorPartido);
  const dataJornada = labelsJornada.map(
    (item) => datos.tarjetasPorPartido[item]
  );
  const mediaJornada = construirMedia(dataJornada);
  const mediaAritemticaJornada = construirMediaAritemtica(dataJornada);
  const atributosJornada = construirAtributos(
    labelsJornada,
    dataJornada,
    mediaJornada
  );
  const oprionsJornadas = constuirOptions("Jornadas", "Número");

  // Parte 1
  const labelsParte1 = Object.keys(datos.partes.parte1);
  const dataParte1 = labelsParte1.map((item) => datos.partes.parte1[item]);
  const mediaParte1 = construirMedia(dataParte1);
  const atributosParte1 = construirAtributos(
    labelsParte1,
    dataParte1,
    mediaParte1
  );
  const oprionsParte1 = constuirOptions("Jornadas", "Número");
  const mediaAritemticaParte1 = construirMediaAritemtica(dataParte1);

  // Parte 2
  const labelsParte2 = Object.keys(datos.partes.parte2);
  const dataParte2 = labelsParte2.map((item) => datos.partes.parte2[item]);
  const mediaParte2 = construirMedia(dataParte2);
  const atributosParte2 = construirAtributos(
    labelsParte2,
    dataParte2,
    mediaParte2
  );
  const mediaAritemticaParte2 = construirMediaAritemtica(dataParte2);
  const oprionsParte2 = constuirOptions("Jornadas", "Número");
  const columns = [
    {
      title: "Jornadas",
      dataIndex: "jornada",
      align: "center",
      key: "jornada",
    },
    {
      title: "Primera parte",
      dataIndex: "parte1",
      align: "center",
      key: "parte1",
    },
    {
      title: "Segunda parte",
      dataIndex: "parte2",
      align: "center",
      key: "parte2",
    },
    {
      title: "Tiempo total",
      align: "center",
      key: "total",
      dataIndex: "total",
    },
  ];

  const data = [
    {
      key: "1",
      jornada: dataJornada.length,
      parte1: datos.partes.num_parte1,
      parte2: datos.partes.num_parte2,
      total: datos.numero,
    },
  ];

  const columnasJugadores = [
    {
      title: "Jugador",
      dataIndex: "nombre",
      align: "center",
      key: "nombre",
    },
    {
      title: "Tarjetas rojas",
      dataIndex: "roja",
      align: "center",
      key: `roja`,
    },
    {
      title: "Tarjetas amarillas",
      dataIndex: "amarilla",
      align: "center",
      key: "amarilla",
    },
  ];

  return (
    <div className="full-space">
      <Layout>
        <h1>Equipos</h1>
        <div className="my-5">
          <Divider>Información general</Divider>
          <Table pagination={false} columns={columns} dataSource={data} />
        </div>
        <div className="my-5">
          <Row gutter={{ xs: 8, sm: 32, md: 32, lg: 32 }} align="center">
            <Col lg={24} md={24} xl={8}>
              <Divider>Información jugadores</Divider>
              <Table
                pagination={false}
                columns={columnasJugadores}
                dataSource={datos.jugadores}
              />
            </Col>
            <Col lg={24} xl={16} md={24} span={24}>
              <Col>
                <div className="my-3">
                  <Divider>Tarjetas</Divider>
                  <Barras datos={atributosJornada} options={oprionsJornadas} />
                </div>
              </Col>
              <Col>
                <div className="mb-3">
                  <Divider>Tajetas en la primera parte</Divider>
                  <Barras datos={atributosParte1} options={oprionsParte1} />
                </div>
              </Col>
              <Col>
                <div className="mb-3">
                  <Divider>Tajetas en la segunda parte</Divider>
                  <Barras datos={atributosParte2} options={oprionsParte2} />
                </div>
              </Col>
            </Col>
          </Row>
        </div>
        <Divider>Información sobre las medias</Divider>
        <Descriptions>
          <Descriptions.Item label="Media">
            {mediaAritemticaJornada}
          </Descriptions.Item>
          <Descriptions.Item label="Media parte 1">
            {mediaAritemticaParte1}
          </Descriptions.Item>
          <Descriptions.Item label="Media parte 2">
            {mediaAritemticaParte2}
          </Descriptions.Item>
          <Descriptions.Item label="Tarjetas como local">
            {datos.lugar.local}
          </Descriptions.Item>
          <Descriptions.Item label="Tarjetas como visitante">
            {datos.lugar.visitante}
          </Descriptions.Item>
        </Descriptions>
      </Layout>
    </div>
  );
};

EquipoTarjetas.getInitialProps = async (params) => {
  const { anio, competicion, equipo } = params.query;

  const { data } = await axios.get(
    `${config.apiURL}tarjetas/competicion/${competicion}/anio/${anio}/equipo/${equipo}`
  );

  if (data.OK) {
    return {
      datos: data.data,
    };
  }
  return { data: {} };
};
export default EquipoTarjetas;
