import React, { useState } from "react";
import Layout from "../components/Layout";
import InputLines from "../components/Inputs/InputLines";
import { createFile } from "../utils/createFile";
import { Divider, Typography, Button } from "antd";
const { Text, Title } = Typography;

const Admin = () => {
  const [event, setEvent] = useState("");
  const [incident, setIncident] = useState("");
  const [odds, setOdds] = useState("");
  const [statistics, setStatistics] = useState("");
  const [error, setError] = useState(null);

  const guardarArchivo = async () => {
    try {
      const parseEvent = JSON.parse(event);
      const parseIncidente = JSON.parse(incident);
      const parseOdds = JSON.parse(odds);
      const parseStatistics = JSON.parse(statistics);
      const json = {
        ...parseEvent,
        ...parseIncidente,
        ...parseStatistics,
        odds: parseOdds,
      };
      await createFile(json);
      setError(null);
      setEvent("");
      setIncident("");
      setStatistics("");
      setOdds("");
    } catch (e) {
      console.log({ e });
      setError("Error en el parseo");
    }
  };

  return (
    <div className="full-space">
      <Layout>
        <Divider style={{ color: "white" }} orientation="left">
          Event
        </Divider>
        <InputLines value={event} onChange={setEvent} />
        <Divider style={{ color: "white" }} orientation="left">
          Incident
        </Divider>
        <InputLines value={incident} onChange={setIncident} />
        <Divider style={{ color: "white" }} orientation="left">
          Odds
        </Divider>
        <InputLines value={odds} onChange={setOdds} />
        <Divider style={{ color: "white" }} orientation="left">
          Statistics
        </Divider>
        <InputLines value={statistics} onChange={setStatistics} />
        <div className="my-5 ">
          <Button
            size="large"
            type="primary"
            disabled={
              event === "" ||
              incident === "" ||
              odds === "" ||
              statistics === ""
            }
            onClick={() => guardarArchivo()}
          >
            Guardar
          </Button>
        </div>
        {error && (
          <Title type="danger" level={2}>
            {error}
          </Title>
        )}
      </Layout>
    </div>
  );
};
export default Admin;
