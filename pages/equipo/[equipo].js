import React from "react";
import Layout from "../../components/Layout";
import axios from "axios";
import config from "../../config/config";

const Equipo = ({ equipo }) => {
  if (!equipo) {
    return null;
  }
  console.log(equipo);

  return (
    <div className="full-space">
      <Layout>
        <h1>{equipo.equipo.nombre}</h1>
      </Layout>
    </div>
  );
};
export default Equipo;

export async function getStaticProps({ params, preview = false }) {
  const { equipo } = params;

  const { data } = await axios.get(`${config.apiURL}equipos/${equipo}`);

  return {
    props: { equipo: data.data },
  };
}

export async function getStaticPaths() {
  const { data } = await axios.get(`${config.apiURL}equipos`);

  return {
    paths: data.data.map((equipo) => `/equipo/${equipo.slug}`) || [],
    fallback: true,
  };
}
