import React from "react";
import Layout from "../components/Layout";

const Index = () => {
  return (
    <div className="full-space">
      <Layout />
    </div>
  );
};
export default Index;
